package com.infostretch.spring.autowired;

import org.springframework.stereotype.Component;

@Component
public class Employees {
	String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
