package com.infostretch.spring.autowired;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.infostretch.spring.autowired")
public class AppConfig {
	@Bean
	Users users() {
		return new Users();
	}
}
