package com.infostretch.spring.autowired;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		Users users = context.getBean(Users.class);
		Employees employees=context.getBean(Employees.class);
		users.setId(888);
		users.setName("Kartik");
		employees.setName("Abhijit");
		System.out.println(users.getId()+"==>"+users.getName()+"==>"+users.getEmployees().name);
	}
}
