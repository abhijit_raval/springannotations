package com.infostretch.spring.primary;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class AppConfig {

	@Primary
	@Bean
	Details abhijitDetails() {
		return new AbhijitDetails();
	}

	@Bean
	Details kartikDetails() {
		return new KartikDetails();
	}
}
