package com.infostretch.spring.primary;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext applicationContext=new AnnotationConfigApplicationContext(AppConfig.class);
		Details details=applicationContext.getBean(Details.class);
		details.myDetails();
	}

}
