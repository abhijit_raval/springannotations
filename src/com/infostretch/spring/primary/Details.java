package com.infostretch.spring.primary;

import org.springframework.stereotype.Component;

@Component
public interface Details {
	void myDetails();
}
