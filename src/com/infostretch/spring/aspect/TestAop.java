package com.infostretch.spring.aspect;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestAop {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		Manager aspect = context.getBean(Manager.class);
		aspect.Justprint();
		aspect.Just();
	}
}
