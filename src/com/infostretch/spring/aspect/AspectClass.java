package com.infostretch.spring.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AspectClass {

	@Pointcut("execution(* Manager.*(..))") //Point-cut expression  
	public void k() {
	}

	@Before("k()")
	void logBeforeV1() {
		System.out.println("Hi");
	}

	@After("k()")
	void logBeforeV2() {
		System.out.println("Hello");
	}
}
