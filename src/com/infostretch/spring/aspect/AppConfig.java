package com.infostretch.spring.aspect;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Primary;

@Configuration
@EnableAspectJAutoProxy
@ComponentScan(basePackages ="com.infostretch.spring.aspect")
public class AppConfig {
	@Primary
	@Bean
	Manager aspect() {
		return new Manager();
	}
}
