package com.infostretch.spring.lazy;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
	public static void main(String[] args) {
		ApplicationContext context=new AnnotationConfigApplicationContext(AppConfig.class);
		Employee employee=context.getBean(Employee.class);
		employee.setId(555);
		employee.setName("Abhi");
		System.out.println(employee.getId()+"==>"+employee.getName());
	}
}
