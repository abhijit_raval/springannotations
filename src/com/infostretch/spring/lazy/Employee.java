package com.infostretch.spring.lazy;

import org.springframework.stereotype.Component;

@Component
public class Employee {

	String name;
	int id;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Employee() {
		System.out.println("Employee initialized");
	}
}
