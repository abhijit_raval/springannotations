package com.infostretch.spring.lazy;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class AppConfig {
	
	@Lazy
	@Bean
	Employee employee() {
		return new Employee();
	}
	
	@Bean
	Books books() {
		return new Books();
	}
}
