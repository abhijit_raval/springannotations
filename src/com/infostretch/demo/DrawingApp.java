package com.infostretch.demo;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.FileSystemResource;

public class DrawingApp implements BeanPostProcessor {
 
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext context=new FileSystemXmlApplicationContext("spring.xml");
//		Shape triangle=(Triangle) context.getBean("triangle");
//		Shape circle=(Circle)context.getBean("circle");
//		triangle.draw();
//		circle.draw();
		/*
		 * DetailsShow bean=(DetailsShow) context.getBean("detailsshow"); bean.show();
		 */
		PersonalDetails details=(PersonalDetails)context.getBean("personaldetails");
		details.show();
	}

}
