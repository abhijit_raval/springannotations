package com.infostretch.demo;

import java.util.List;

public class DetailsShow {
	
	List<DetailsBean> beans;

	public List<DetailsBean> getBeans() {
		return beans;
	}

	public void setBeans(List<DetailsBean> beans) {
		this.beans = beans;
	}

	public void show() {
		for (DetailsBean detailsBean : beans) {
			System.out.println(detailsBean.getName()+"===>"+detailsBean.getAge());
		}
	}
}
