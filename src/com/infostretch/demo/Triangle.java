package com.infostretch.demo;

public class Triangle implements Shape {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void draw() {
		// TODO Auto-generated method stub
		System.out.println("My name is "+name);
		System.out.println("This is triangle");
	}
}
