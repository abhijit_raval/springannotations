package com.infostretch.demo;

public class Circle implements Shape {

	String name;

	public Circle(String name) {
		this.name = name;
	}

	@Override
	public void draw() {
		// TODO Auto-generated method stub
		System.out.println("This is circle");
		System.out.println("This is "+name);
	}
}
