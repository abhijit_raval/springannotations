package com.springdemo.demo;

import org.springframework.stereotype.Controller;

@Controller
public class Employee {

	String name;
	int id;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public Employee(String name, int id) {
		super();
		System.out.println("In employee");
		this.name = name;
		this.id = id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
