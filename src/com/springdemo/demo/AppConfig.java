package com.springdemo.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
@ComponentScan(basePackages = "com.springdemo")
public class AppConfig {
	@Autowired
	Employee employee;
	@Bean
	public Employee employee(){
		return new Employee("Abhi",222);
	}
//	@Bean
//	@Primary
//	public Employee employee2() {
//		return new Employee("KArtik",333);
//	}
}
